#ifndef CUBE_H
#define CUBE_H

#include <GL/gl.h>

#define SIZE_BORDER 4
#define SIZE_BORDER_CUBE 6
#define STEP .01f
#define MAX_STEP 20

enum TYPE_STEP {
    LEFT = 1,
    RIGHT = 2,
    UP = 3,
    DOWN = 4,
    EXTEN = 0,
};

struct RGBA {
    float r;
    float g;
    float b;
    float a;
};

struct Border {
    float point[SIZE_BORDER * 3]; 
    struct RGBA color;
};

struct Cube {
    struct Border border_cub[SIZE_BORDER_CUBE];
    int step_size;
    int alpha_x;
    int alpha_y;
    GLuint texturs[SIZE_BORDER_CUBE];
    int on_texturs;
    void (*draw_cube)(const struct Cube*);
    void (*step)(struct Cube*, enum TYPE_STEP);
    
}; 

void init_cube(struct Cube* value, float size);

#endif
