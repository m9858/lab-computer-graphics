#include "cube.h"
#include <string.h>
#include <SDL2/SDL_image.h> 

static GLuint init_texture(char* name) {
     SDL_Surface* image = IMG_Load(name);
     if ( image == 0 ) {
        fprintf(stderr, "Error: %s!\n", name);
        exit(1);
     }
     GLuint texture;
     glGenTextures(1, &texture);
     glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
     SDL_FreeSurface(image);
     glBindTexture(GL_TEXTURE_2D, 0);
     return texture;
}


static void draw(const struct Cube* value) {
    if ( value->on_texturs ) {
        glEnable(GL_TEXTURE_2D);
    }
    float x[] = {0, 1,  1, 1,  1, 0,  0, 0};
    for (int i = 0; i < SIZE_BORDER_CUBE; i++) {
        struct RGBA rgba = value->border_cub[i].color;
        if ( value->on_texturs ) {
            glColor3f(1, 1, 1);
            glBindTexture(GL_TEXTURE_2D, value->texturs[i]);
        }
        else {
            glColor4f(rgba.r,rgba.g, rgba.b, rgba.a);
        }
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_NORMAL_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glVertexPointer(3, GL_FLOAT, 0, value->border_cub[i].point); 
            glNormalPointer(GL_FLOAT, 0, value->border_cub[i].point);
            if ( value->on_texturs ) {
                glTexCoordPointer(2, GL_FLOAT, 0, x);
            }
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4); 
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);        
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY); 
    }
    if ( value->on_texturs ) {
        glDisable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    return ;
}

static void spreading_shift(struct Cube* value, int shift) {
    for (int i = 0; i < SIZE_BORDER * 3; i += 3) { 
        value->border_cub[2].point[i] += STEP * shift;
        value->border_cub[3].point[i] -= STEP * shift;
        value->border_cub[4].point[i + 1] += STEP * shift;
        value->border_cub[5].point[i + 1] -= STEP * shift;
        value->border_cub[0].point[i + 2] += STEP * shift;
        value->border_cub[1].point[i + 2] -= STEP * shift;
    }
    return ;
}    

static void step(struct Cube* value, enum TYPE_STEP value_step) {
    switch ( value_step ) {
        case LEFT :
            value->alpha_y -= 1;
            break;
        case RIGHT :
            value->alpha_y += 1;
            break;
        case UP :
            value->alpha_x += 1;
            break;
        case DOWN:
            value->alpha_x -= 1;
            break;
        case EXTEN :
            value->step_size += 1;
            spreading_shift(value, value->step_size > MAX_STEP ? 1 : -1);
            value->step_size %= MAX_STEP * 2;
            break;
    }
    return ;
}

void init_cube(struct Cube* value, float size) {
    value->step_size = 0;
    value->alpha_x = -16;
    value->alpha_y = 22;
    value->draw_cube = draw;
    value->step = step;
    value->on_texturs = 0;
    float arr_sign[] = {-1, -1, -1,  1, -1, -1, 1, 1, -1, -1, 1, -1,
                        1, -1, 1, -1, -1, 1, -1, 1, 1, 1, 1, 1,
                        -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, 1, 
                        1, -1, -1, 1, -1, 1, 1, 1, 1, 1, 1, -1,
                        1, -1, 1, -1, -1, 1, -1, -1, -1, 1, -1, -1 ,
                        -1,  1, 1, 1, 1, 1, 1, 1, -1, -1, 1, -1 };

    struct RGBA color[] = {{1, 0, 0, .5}, {0, 1, 0, .5}, {0, 0, 1, .5}, {1, 0, 1, .5}, 
                          {1, 1, 0, .5}, {0, 1, 1, .5}};
    int arr_coint = 0;
    for (int i = 0; i < SIZE_BORDER_CUBE; i++) {
        value->border_cub[i].color = color[i];
        for (int j = 0; j < SIZE_BORDER * 3; j++, arr_coint++) {
            value->border_cub[i].point[j] = size * arr_sign[arr_coint];
        }
    }
    for (int i = 0; i < SIZE_BORDER_CUBE; i++) {
         char name[100] = "terturx/";
         name[8] = i + 1 + '0';
         strcat(name, ".jpg");
         value->texturs[i] = init_texture(name);
    }
    return ;
} 
