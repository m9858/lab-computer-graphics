#include <GLFW/glfw3.h>
#include <unistd.h>
#include "scene.h"

static void key_callback(GLFWwindow* window, int key, int scancode, int action, 
		         int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    return ;
}

int main() {

    if (!glfwInit())
        return -1;
    GLFWwindow* window = glfwCreateWindow(640, 480, "Lab 1", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }
    glfwSetWindowSizeLimits(window, 640, 480, 640, 480);
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);

    struct scene anim_scene;
    constructor_scene(&anim_scene);
    while (!glfwWindowShouldClose(window)) {
	struct RGB fon = anim_scene.f_background(&anim_scene);
	glClearColor(fon.r, fon.b, fon.g, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);

	anim_scene.f_draw(&anim_scene);
	anim_scene.f_step(&anim_scene);
        glfwSwapBuffers(window);
        glfwPollEvents();
	usleep(200000);
	
    }
    glfwDestroyWindow(window); 
    glfwTerminate();
    return 0;
}
