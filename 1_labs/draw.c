#include "draw.h"
#include <GLFW/glfw3.h>
#include <math.h>
#include <stdio.h>


struct RGB dimmingRGB(const struct RGB* value) {
    struct RGB e = *value;
    e.r *= DIMMING_R;
    e.g *= DIMMING_G;
    e.b *= DIMMING_B;
    return e;
}

void drawLine(const struct Line* value) {
    glBegin(GL_LINES);
        glLineWidth(5);
        glColor3f(value->color.r, value->color.g, value->color.b);
	glVertex2f(value->x, value->y);
	glVertex2f(value->x1, value->y1);
    glEnd();
    return ;
}

void drawRect(const struct Rect* value) {
    glBegin(GL_TRIANGLE_FAN);
        glColor3f(value->color.r, value->color.g, value->color.b);
	glVertex2f(value->x, value->y); 
        glVertex2f(value->x + value->width, value->y); 
        glVertex2f(value->x + value->width, value->y - value->height); 
        glVertex2f(value->x, value->y - value->height); 
    glEnd();
    return ;
}

void drawElepse(const struct Elepse* value) {
    int segments = 100;
    glBegin(GL_TRIANGLE_FAN);
	glColor3f(value->color.r, value->color.g, value->color.b);
	glVertex2f(value->x, value->y);
	for (int i = 0; i <= segments; i++) {
	    float angle = i * 2.0f * M_PI / segments;
	    float dx = value->radius * cos(angle);
	    float dy = value->radius * sin(angle);
	    glVertex2f(value->x_sprains * dx + value->x, 
                       value->y_sprains * dy + value->y);
	}
    glEnd();
    return;
}

void drawFlower(const struct Rect* value) {
    float width = value->width / 10;
    float x = value->x + value->width / 2 - width / 2;
    float height = value->height / 1.5; 
    float y = value->y - (value->height - height);
    struct Rect stem = {x, y, width, height, {0.13, 0.13, 0.56}};
    float rad = value->width / 4;
    x = value->x + value->width / 2;
    y += rad;
    struct Elepse center = {x, y, rad, {0.5, 0.5, 0}, 1, 1};
    drawRect(&stem);
    float color = 0.4;
    for (int i = 0; i < 360; i += 60, color += 0.1) {
	float angle = i * M_PI  / 180;
	float xd = cos(angle) * rad + x;
	float yd = sin(angle) * rad + y;
        struct Elepse petal = {xd , yd , rad , {0, color, color}, 1, 1};
	drawElepse(&petal);
    }
    drawElepse(&center);
    rad /= 2;
    x = value->x + value->width / 2 - width / 2 - rad * 1.9;
    y = value->y - (value->height - height / 2);
    struct Elepse list = {x, y, rad, {0.13, 0.13, 0.56}, 1.9, 1};
    drawElepse(&list);
    return ;
}

void drawFolder(const struct Elepse* value, int dimming) {
   struct Elepse e = *value;
   e.color = dimming ? dimmingRGB(&e.color) : e.color;
   drawElepse(&e);
   return ;
}

void drawSun(const struct Elepse* value) {
    struct Line line = {value->x, value->y, 0, 0, value->color};
    drawElepse(value);
    for (int i = 0; i < 360; i += 30) {
	float angle = i * M_PI  / 180;
	float dx = value->radius * cos(angle);
	float dy = value->radius * sin(angle);
	line.x1 = 1.3 * dx + value->x;
	line.y1 = 1.3 * dy + value->y;
	drawLine(&line);
    }
    return ;
}

void drawGrass(const struct Rect* value, int dimming) {
    glLineWidth(2.0f);
    struct RGB color = value->color;
    color = dimming ? dimmingRGB(&color) : color;
    float centerer = value->width / 2;
    struct Line line = {value->x, value->y, value->x + centerer, value->y - value->height, color};
    drawLine(&line);
    line.x += centerer;
    drawLine(&line);
    line.x += centerer;
    drawLine(&line);
    return ;
}

void drawTriangle(const struct Triangle* value) {
    glBegin(GL_TRIANGLES); 
      glColor3f(value->color.r, value->color.g, value->color.b);
      glVertex2f(value->x1, value->y1);
      glVertex2f(value->x2, value->y2);
      glVertex2f(value->x3, value->y3);
    glEnd();
    return ;
}


void drawStar(const struct Rect* value) {
    struct RGB color = {0.5, 0.5, 0.6} ;
    float x = value->width / 2;
    float y = value->height / 2;

    float size_x = value->width / 4;
    float size_y = value->height / 4;
    
    struct Triangle ex = {x + value->x , value->y, 
	                 x + value->x - size_x / 2, value->y - y,
			 x + value->x + size_x / 2, value->y - y, color};

    drawTriangle(&ex);
    ex.y1 = value->y - value->height ;
    drawTriangle(&ex);

    struct Triangle ey = {value->x , value->y - y, 
	                 x + value->x, value->y - y + size_y / 2,
			 x + value->x, value->y - y - size_y / 2, color};
    drawTriangle(&ey);
    ey.x1 = value->x + value->width;
    drawTriangle(&ey);
    return ;
}

    
      
