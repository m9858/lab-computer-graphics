#ifndef DRAW_H
#define DRAW_H

#define DIMMING_R 0.5f;
#define DIMMING_G 0.5f;
#define DIMMING_B 0.7f;

struct RGB {
    float r;
    float g;
    float b;
};

struct Line {
   float x;
   float y;
   float x1;
   float y1;
   struct RGB color;
};

struct Elepse {
    float x;
    float y;
    float radius;
    struct RGB color; 

    float x_sprains;
    float y_sprains;
};

struct Rect {
    float x;
    float y;
    float width;
    float height;

    struct RGB color;
};

struct Triangle {
   float x1;
   float y1;
   float x2;
   float y2;
   float x3;
   float y3;

   struct RGB color;
};


struct RGB dimmingRGB(const struct RGB* value);
void drawLine(const struct Line* value);
void drawElepse(const struct Elepse* value);
void drawRect(const struct Rect* value); 
void drawtriangle(const struct Triangle* value);
void drawFlower(const struct Rect* value);
void drawStar(const struct Rect* value);
void drawSun(const struct Elepse* value);
void drawFolder(const struct Elepse* value, int dimming);
void drawGrass(const struct Rect* value, int dimming);

#endif
