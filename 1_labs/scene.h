#ifndef SCENE_H
#define SCENE_H

#include "draw.h"

#define A_SUN_DEFAUL 160
#define A_MOON_DEFAUL 300
#define A_DAY_DEFAULT 140
#define ANGLE_LIMIT 280
#define RADIUS 0.6
#define RADIUS_ELEPS 0.1
#define STEP 5.f
#define SUPPLIES_X 1.96 
#define background_r 0
#define background_b 0.5
#define background_g 0

struct scene {
    float a_sun; 
    float a_moon; 
    float a_day; 
    float a_sun_min; 
    float a_moon_min; 
    float a_day_min; 
    float step; 
    float radius;
    struct Elepse sun;
    struct Elepse moon;
    struct RGB background;

    struct RGB (*f_background)(const struct scene* );
    void (*f_draw)(struct scene* );
    void (*f_step)(struct scene* );

    
};

void constructor_scene(struct scene* value);

#endif 
