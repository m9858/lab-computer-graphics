#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <sys/signal.h>
#include "cube.h"
#include <unistd.h>

static void print_describe() {
    printf("\tУправления\n");
    printf("SPACE - расдвинуть грани и сдвинуть\n");
    printf("Стрелочка вправо - поворачивает вправо\n");
    printf("Стрелочка влево - поворачивает влево\n");
    printf("Стрелочка вниз - поворачивает вниз\n");
    printf("Стрелочка вверх - поворачивает вверх\n");
    printf("Кнопка 1 - вк\\выкл освещения\n");
    printf("Enter - остановка и запус лампы\n");
    printf("Кнопка 2 - вк\\выкл текстур\n");
    return ;
}


int main(void) {
    SDL_Init(SDL_INIT_EVERYTHING);
    signal(SIGINT, SIG_DFL);
    SDL_Window* win = SDL_CreateWindow("Lab 4", 0, 0, 800, 500, SDL_WINDOW_OPENGL);
    SDL_GLContext glc = SDL_GL_CreateContext(win);
    SDL_GL_MakeCurrent(win, glc);

    print_describe();   

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);     
    glEnable(GL_NORMALIZE); 
    glEnable(GL_COLOR_MATERIAL);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    struct Cube value;
    struct Cube value2;
    init_cube(&value, .3f);
    init_cube(&value2, .05f);
    int i = 180;  
    int on_off_lighting = 0;
    int step_lighting = 0;
    while ( 1 ) {
        SDL_Event e;
        while( SDL_PollEvent(&e) != 0 ) {
            if (e.type == SDL_QUIT) {
                goto quit; //что то мне как то влом это на что то дурго заменять
            }
            if (e.type == SDL_KEYDOWN) {
                if (e.key.keysym.sym == SDLK_RIGHT) {
                    value.step(&value, RIGHT);
                }
                if (e.key.keysym.sym == SDLK_LEFT) {
                    value.step(&value, LEFT);
                }
                if (e.key.keysym.sym == SDLK_UP) {
                    value.step(&value, UP);
                }
                if (e.key.keysym.sym == SDLK_DOWN) {
                    value.step(&value, DOWN);
                }
                if (e.key.keysym.sym == SDLK_SPACE) {
                    value.step(&value, EXTEN);
                }
                if (e.key.keysym.sym == SDLK_1) {
                    on_off_lighting = !on_off_lighting;
                }
                if (e.key.keysym.sym == SDLK_2) {
                    value.on_texturs = !value.on_texturs;
                }

                if (e.key.keysym.sym == SDLK_RETURN ) {
                    step_lighting = !step_lighting;
                } 
            }
        }
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        glRotatef(value.alpha_y, 0, 1, 0);
        glRotatef(value.alpha_x, 1, 0, 0);
        
        glPushMatrix();
            glRotatef(i, 0, 1, 0);
            GLfloat light0_direction[] = {0.0, 0.0, 0.7, 0.0}; 
            glLightfv(GL_LIGHT0, GL_POSITION, light0_direction);
            glTranslatef(0, 0, 0.7);
            value2.draw_cube(&value2);                  
        glPopMatrix();

        if ( on_off_lighting  ) {
           glEnable(GL_LIGHT0); 

        }
        value.draw_cube(&value);  
        if ( on_off_lighting  ) {
           glDisable(GL_LIGHT0);
        }
        SDL_GL_SwapWindow(win);
        i += step_lighting;
        i %= 360;
        usleep(1000);
    }
quit:
    SDL_DestroyWindow(win);
    SDL_Quit();
}   
