#include "cube.h"
#include <GL/gl.h>


static void draw(const struct Cube* value) {
    for (int i = 0; i < SIZE_BORDER_CUBE; i++) {
        struct RGB rgb = value->border_cub[i].color;
        glColor3f(rgb.r, rgb.g, rgb.b);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_FLOAT, 0, value->border_cub[i].point);
            glVertexPointer(3, GL_FLOAT, 0, value->border_cub[i].point); 
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4); 
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY); 

        glEnableClientState(GL_VERTEX_ARRAY);
            float line[] = { value->border_cub[i].point[0], 
value->border_cub[i].point[1], value->border_cub[i].point[2],
value->border_cub[i].point[9], value->border_cub[i].point[10], 
value->border_cub[i].point[11], value->border_cub[i].point[3], 
value->border_cub[i].point[4], value->border_cub[i].point[5],
value->border_cub[i].point[6], value->border_cub[i].point[7], 
value->border_cub[i].point[8]};
            glVertexPointer(3, GL_FLOAT, 0, line); 
            glDrawArrays(GL_LINES, 0, 4); 
        glDisableClientState(GL_VERTEX_ARRAY); 
    }
    return ;
}

static void spreading_shift(struct Cube* value, int shift) {
    for (int i = 0; i < SIZE_BORDER * 3; i += 3) { 
        value->border_cub[2].point[i] += STEP * shift;
        value->border_cub[3].point[i] -= STEP * shift;
        value->border_cub[4].point[i + 1] += STEP * shift;
        value->border_cub[5].point[i + 1] -= STEP * shift;
        value->border_cub[0].point[i + 2] -= STEP * shift;
        value->border_cub[1].point[i + 2] += STEP * shift;
    }
    return ;
}    

static void step(struct Cube* value, enum TYPE_STEP value_step) {
    switch ( value_step ) {
        case LEFT :
            value->alpha_y -= 1;
            break;
        case RIGHT :
            value->alpha_y += 1;
            break;
        case UP :
            value->alpha_x += 1;
            break;
        case DOWN:
            value->alpha_x -= 1;
            break;
        case EXTEN :
            value->step_size += 1;
            spreading_shift(value, value->step_size > MAX_STEP ? 1 : -1);
            value->step_size %= MAX_STEP * 2;
            break;
    }
    return ;
}

void init_cube(struct Cube* value, float size) {
    value->step_size = 0;
    value->alpha_x = 0;
    value->alpha_y = 0;
    value->draw_cube = draw;
    value->step = step;
    float arr_sign[] = {-1, -1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1,
                        1, -1, -1, -1, -1, -1, -1, 1, -1, 1, 1, -1,
                        -1, -1, -1, -1, -1, 1, -1, 1, 1, -1, 1, -1,
                        1, -1, 1,  1, -1, -1,  1, 1, -1, 1, 1, 1,
                        -1, -1, 1,  1, -1, 1,  1, -1, -1,  -1, -1, -1,
                        -1,  1, 1,  1, 1, 1,  1, 1, -1, -1, 1, -1 };
    struct RGB color[] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {1, 0, 1}, 
                          {1, 1, 0}, {0, 1, 1}};
    int arr_coint = 0;
    for (int i = 0; i < SIZE_BORDER_CUBE; i++) {
        value->border_cub[i].color = color[i];
        for (int j = 0; j < SIZE_BORDER * 3; j++, arr_coint++) {
            value->border_cub[i].point[j] = size * arr_sign[arr_coint];
        }
    }
}
