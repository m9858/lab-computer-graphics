#ifndef DRAW_H
#define DRAW_H

#include <GLFW/glfw3.h>

#define DIMMING_R 0.5f
#define DIMMING_G 0.5f
#define DIMMING_B 0.7f

struct RGB {
    GLfloat r;
    GLfloat g;
    GLfloat b;
};

struct Line {
   GLfloat x;
   GLfloat y;
   GLfloat x1;
   GLfloat y1;
   struct RGB color;
};

struct Elepse {
    GLfloat x;
    GLfloat y;
    GLfloat radius;
    struct RGB color; 

    GLfloat x_sprains;
    GLfloat y_sprains;
};

struct Rect {
    GLfloat x;
    GLfloat y;
    GLfloat width;
    GLfloat height;

    struct RGB color;
};

struct Triangle {
   GLfloat x1;
   GLfloat y1;
   GLfloat x2;
   GLfloat y2;
   GLfloat x3;
   GLfloat y3;

   struct RGB color;
};




struct RGB dimmingRGB(const struct RGB* value);
void drawLine(const struct Line* value);
void drawElepse(const struct Elepse* value);
void drawRect(const struct Rect* value); 
void drawtriangle(const struct Triangle* value);
void drawFlower(const struct Rect* value);
void drawStar(const struct Rect* value);
void drawSun(const struct Elepse* value);
void drawFolder(const struct Elepse* value, int dimming);
void drawGrass(const struct Rect* value, int dimming);

#endif
