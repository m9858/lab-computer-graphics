#include "draw.h"
#include <math.h>
#include <stdio.h>

struct RGB dimmingRGB(const struct RGB* value) {
    struct RGB e = *value;
    e.r *= DIMMING_R;
    e.g *= DIMMING_G;
    e.b *= DIMMING_B;
    return e;
}

void drawLine(const struct Line* value) {
    GLfloat vertex[2][2] = { {value->x, value->y},
	                     {value->x1, value->y1} };
    GLfloat color[2][3] = { {value->color.r, value->color.g, value->color.b},
	                    {value->color.r, value->color.g, value->color.b} };
    glVertexPointer(2, GL_FLOAT, 0, vertex);
    glColorPointer(3, GL_FLOAT, 0, color);
    glDrawArrays(GL_LINES, 0, 2); 
    return ;
}

void drawRect(const struct Rect* value) {
    GLfloat vertex[4][2] = {{value->x, value->y},
	                {value->x + value->width, value->y},
			{value->x + value->width, value->y - value->height},
			{value->x, value->y - value->height}};
    GLfloat color[4][3] = { {value->color.r, value->color.g, value->color.b},
	                    {value->color.r, value->color.g, value->color.b},
                            {value->color.r, value->color.g, value->color.b},
                            {value->color.r, value->color.g, value->color.b} };
    glVertexPointer(2, GL_FLOAT, 0, vertex);
    glColorPointer(3, GL_FLOAT, 0, color);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4); 
    return ;
}

void drawElepse(const struct Elepse* value) {
    int segments = 100;
    GLfloat vertex[segments + 1][2];
    GLfloat color[segments + 1][3];
    for (int i = 0; i <= segments; i++) {
	color[i][0] = value->color.r;
        color[i][1] = value->color.g; 
        color[i][2] = value->color.b;
        GLfloat angle = (i) * 2.0f * M_PI / segments;
        GLfloat dx = value->radius * cos(angle);
	float dy = value->radius * sin(angle);
	vertex[i][0] = value->x_sprains * dx + value->x;
        vertex[i][1] = value->y_sprains * dy + value->y;
    }
    glVertexPointer(2, GL_FLOAT, 0, vertex);
    glColorPointer(3, GL_FLOAT, 0, color);
    glDrawArrays(GL_TRIANGLE_FAN, 0, segments + 1); 
    return;
}

void drawFlower(const struct Rect* value) {
    GLfloat width = value->width / 10;
    GLfloat x = value->x + value->width / 2 - width / 2;
    GLfloat height = value->height / 1.5; 
    GLfloat y = value->y - (value->height - height);
    struct Rect stem = {x, y, width, height, {0.13, 0.13, 0.56}};
    GLfloat rad = value->width / 4;
    x = value->x + value->width / 2;
    y += rad;
    struct Elepse center = {x, y, rad, {0.5, 0.5, 0}, 1, 1};
    drawRect(&stem);
    GLfloat color = 0.4;
    for (int i = 0; i < 360; i += 60, color += 0.1) {
	float angle = i * M_PI  / 180;
	float xd = cos(angle) * rad + x;
	float yd = sin(angle) * rad + y;
        struct Elepse petal = {xd , yd , rad , {0, color, color}, 1, 1};
	drawElepse(&petal);
    }
    drawElepse(&center);
    rad /= 2;
    x = value->x + value->width / 2 - width / 2 - rad * 1.9;
    y = value->y - (value->height - height / 2);
    struct Elepse list = {x, y, rad, {0.13, 0.13, 0.56}, 1.9, 1};
    drawElepse(&list);
    return ;
}

void drawFolder(const struct Elepse* value, int dimming) {
   struct Elepse e = *value;
   e.color = dimming ? dimmingRGB(&e.color) : e.color;
   drawElepse(&e);
   return ;
}

void drawSun(const struct Elepse* value) {
    struct Line line = {value->x, value->y, 0, 0, value->color};
    drawElepse(value);
    for (int i = 0; i < 360; i += 30) {
	float angle = i * M_PI  / 180;
	float dx = value->radius * cos(angle);
	float dy = value->radius * sin(angle);
	line.x1 = 1.3 * dx + value->x;
	line.y1 = 1.3 * dy + value->y;
	drawLine(&line);
    }
    return ;
}

void drawGrass(const struct Rect* value, int dimming) {
    glLineWidth(2.0f);
    struct RGB color = value->color;
    color = dimming ? dimmingRGB(&color) : color;
    GLfloat centerer = value->width / 2;
    struct Line line = {value->x, value->y, value->x + centerer, value->y - value->height, color};
    drawLine(&line);
    line.x += centerer;
    drawLine(&line);
    line.x += centerer;
    drawLine(&line);
    return ;
}

void drawTriangle(const struct Triangle* value) {
    GLfloat vertex[3][2] = { {value->x1, value->y1},
                             {value->x2, value->y2},
			     {value->x3, value->y3} };
    GLfloat color[3][3] = { {value->color.r, value->color.g, value->color.b},
	                    {value->color.r, value->color.g, value->color.b},
                            {value->color.r, value->color.g, value->color.b} };
    glVertexPointer(2, GL_FLOAT, 0, vertex);
    glColorPointer(3, GL_FLOAT, 0, color);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 3);
    return ;
}

void drawStar(const struct Rect* value) { 
    GLfloat x = value->width / 2;
    GLfloat y = value->height / 2;

    GLfloat size_x = value->width / 4;
    GLfloat size_y = value->height / 4;	                     

    struct RGB color = {0.5, 0.5, 0.6} ;
    
    struct Triangle ex = {x + value->x , value->y, 
	                 x + value->x - size_x / 2, value->y - y,
			 x + value->x + size_x / 2, value->y - y, color};

    drawTriangle(&ex);
    ex.y1 = value->y - value->height ;
    drawTriangle(&ex);

    struct Triangle ey = {value->x , value->y - y, 
	                 x + value->x, value->y - y + size_y / 2,
			 x + value->x, value->y - y - size_y / 2, color};
    drawTriangle(&ey);
    ey.x1 = value->x + value->width;
    drawTriangle(&ey);
    return ;
}
