#include "scene.h"
#include <math.h>

static void f_step(struct scene* value) {
    value->a_sun -= value->step;
    value->a_moon -= value->step;
    value->a_day -= value->step;
    if(value->a_sun < value->a_sun_min)
        value->a_sun += ANGLE_LIMIT;
    if(value->a_moon < value->a_moon_min)
        value->a_moon += ANGLE_LIMIT;
    if(value->a_day < value->a_day_min)
        value->a_day += ANGLE_LIMIT;
    GLfloat theta = M_PI * value->a_sun / 180.0f;
    value->sun.x = value->radius * cos(theta) * SUPPLIES_X;
    value->sun.y = value->radius * sin(theta) - 0.2;
    theta = M_PI * value->a_moon / 180.0f;
    value->moon.x = value->radius * cos(theta) * SUPPLIES_X;
    value->moon.y = value->radius * sin(theta) - 0.2;
    return ;
}

static void f_draw(struct scene* value) {
    struct Elepse clearing = {0, -1, 1, {.5f, 1, 0}, 2, 1};
    drawFolder(&clearing, value->a_day < 0);
    if ( value->a_day < 0 ) {
        drawElepse(&value->moon);
	struct Rect start = {-0.9, 1, 0.1, 0.1};
	for (float i = 1; i >= 0;  i-= 0.3) { 
	    for (float j = -0.9; j < 1; j += 0.4) {
   	         drawStar(&start);
	         start.x += 0.4;
	    }
	    start.x = -0.8 - i * 0.5 ;
	    start.y -= 0.25;
	}
    }
    else {
	drawSun(&value->sun);
    }
    struct Rect grass1 = {-0.5, -0.1, 0.02, 0.04, {0, 0.5, 0}};
    struct Rect grass2 = {-0.5, -0.2, 0.02, 0.04, {0, 0.5, 0}};
    for (float i = 0; i <= 1; i += 0.05) {
        drawGrass(&grass1, value->a_day < 0);
        drawGrass(&grass2, value->a_day < 0);
	grass1.x += 0.05;
	grass2.x += 0.05;
    }
	
    struct Rect flower = {-0.9, -0.8, 0.04, 0.07, {0, 0, 0}};
    for (float j = -0.9; j < -0.5; j += 0.08) {
         for (float i = -1; i < -0.5; i += 0.05) {
	      drawFlower(&flower);
	      flower.x += 0.05;
	 }
	 flower.y += 0.08;
	 flower.x = -0.9;
    }
    struct Elepse pond = {0.5, -0.6, 0.2, {0.0f, 1.0f, 1.0f}, 2, 1};
    pond.color = (value->a_day < 0 ?  dimmingRGB(&pond.color) : pond.color);
    drawElepse(&pond);
    return ;
}

static struct RGB f_background(const struct scene* value) { 
    return value->a_day >= 0 ? value->background :dimmingRGB(&value->background);
}

void constructor_scene(struct scene* value) {
    value->a_sun = A_SUN_DEFAUL;
    value->a_sun_min = value->a_sun - ANGLE_LIMIT;
    value->a_moon = A_MOON_DEFAUL;
    value->a_moon_min = value->a_moon - ANGLE_LIMIT;
    value->a_day = A_DAY_DEFAULT;
    value->a_day_min = value->a_day - ANGLE_LIMIT;
    value->step = STEP;
    value->radius = RADIUS;
    value->background = (struct RGB){0, 0.5, 0};
    
    GLfloat theta = M_PI * value->a_sun / 180.0f;
    value->sun = (struct Elepse){value->radius * cos(theta) * SUPPLIES_X, 
	                         value->radius * sin(theta) - 0.2, 
	                         RADIUS_ELEPS, {1, 1, 0}, 1, 1};

    theta = M_PI * value->a_moon / 180.0f;
    value->moon = (struct Elepse) {value->radius * cos(theta) * SUPPLIES_X, 
	                           value->radius * sin(theta) - 0.2, 
	                           RADIUS_ELEPS, {0.5, 0.5, 0.6}, 1, 1};
    value->f_draw = f_draw;
    value->f_step = f_step;
    value->f_background = f_background;
    return ;
}
